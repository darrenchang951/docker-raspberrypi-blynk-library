FROM gcc:4.9
# install required
RUN set -ex; \
    apt-get update; \
    apt-get -y install build-essential sudo;

# install wiringPi
WORKDIR /usr/src/
RUN set -ex; \
    git clone https://github.com/wiringpi/wiringpi; \
    cd wiringpi; \
    ./build;

# Download Blynk library
RUN set -ex; \
    git clone https://github.com/blynkkk/blynk-library.git;

# Compile
COPY ./blynk/ /usr/src/my_blynk/
WORKDIR /usr/src/blynk-library/linux/
RUN set -ex; \
    cp -r /usr/src/my_blynk/* /usr/src/blynk-library/linux/; \
    make target=raspberry;

CMD ["./blynk"]
