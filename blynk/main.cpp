/**
 * @file       main.cpp
 * @author     Darren Chang
 * @license    This project is released under the MIT License (MIT)
 * @date       Jan 2019
 * @brief
 */

//#define BLYNK_DEBUG
#define BLYNK_PRINT stdout
#ifdef RASPBERRY
#include <BlynkApiWiringPi.h>
#else

#include <BlynkApiLinux.h>

#endif

#include <BlynkSocket.h>
#include <BlynkOptionsParser.h>

static BlynkTransportSocket _blynkTransport;
BlynkSocket Blynk(_blynkTransport);

static const char *auth, *serv;
static uint16_t port;

#include <BlynkWidgets.h>

BlynkTimer tmr;
unsigned int uptime;
unsigned int toggle_count;
bool is_blynk_connected = false;

BLYNK_WRITE(V1) {
        printf("Got a value: %s\n", param[0].asStr());
}

void toggleCount() {
    Blynk.virtualWrite(V2, toggle_count);
}

int *totalTime(int totalSeconds) {
    int result[4];
    result[0] = totalSeconds % 60;
    result[1] = (totalSeconds % (3600)) / 60;
    result[2] = (totalSeconds % 86400) / 3600;
    result[3] = totalSeconds / 86400;
    return result;
}

BLYNK_WRITE(V14) {
        toggle_count++;
        if (param[0] == 0) {
            printf("on\n");
            digitalWrite(14, LOW);
        }
        else {
            printf("off\n");
            digitalWrite(14, HIGH);
        }
}

void setup() {
    Blynk.begin(auth, serv, port);
    while (Blynk.connect() == false) {
	}
	is_blynk_connected = true;
	Blynk.notify("I'm online!");
	digitalWrite(14, HIGH);
    tmr.setInterval(1000, []() {
        int totalSeconds = BlynkMillis() / 1000;
        Blynk.virtualWrite(V0,
                           totalSeconds / 86400, " Days ",
                           (totalSeconds % 86400) / 3600, " Hours ",
                           (totalSeconds % (3600)) / 60, " Minutes ",
                           totalSeconds % 60, " Seconds");
    });
}

void loop() {
    if (is_blynk_connected == false) {
        while (Blynk.connect() == false) {
        }
        is_blynk_connected = true;
        Blynk.notify("I'm reconnected!");
    }
    Blynk.run();
    tmr.run();
    toggleCount();
    is_blynk_connected = Blynk.connected();
}

int main(int argc, char *argv[]) {
    parse_options(argc, argv, auth, serv, port);
    setup();
    while (true) {
        loop();
    }
    return 0;
}
