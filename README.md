# Light toggle
A Blynk application to toggle on and off an electrical application (e.g. lamp). This project is built using Blynk.
You can visite <http://docs.blynk.cc/> for Blynk documentation.

# How to use
1. Build the image using `Dockerfile`.
```
docker build -t lamp .
```

2. Start a container using the image. Instruction to get `your_auth_token` at <https://www.blynk.cc/getting-started/> 
```
docker run -d --restart always --device /dev/gpiomem --name lamp \
lamp ./blynk -t auth_token
```
